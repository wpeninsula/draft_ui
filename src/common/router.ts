import { Routes } from '@angular/router';
import { TaskComponent } from '../app/components/task/task.component';

export const appRoutes : Routes = [
    { path: 'task', component: TaskComponent }
];