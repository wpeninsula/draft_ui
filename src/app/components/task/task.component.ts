import { Component, OnInit, ViewChild } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { TaskAddComponent } from './task-add/task-add.component';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
    taskColumnNames = ['taskId', 'taskName', 'state', 'fileName', 'repeat', 'from', 'to', 'time', 'actions'];    
    dataSource: MatTableDataSource<Task>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(public taskService: TaskService, private dialog: MatDialog) { 
        this.dataSource = new MatTableDataSource([]);
        this.getDataSource();   
    }

    ngOnInit() {
    }

    getDataSource() {
        this.taskService.getAllTasks().subscribe((tasks) => {
            console.log(tasks)
            this.dataSource = new MatTableDataSource(tasks);           
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        });
    }

    addTask(task: Task) {
        const dialogRef = this.dialog.open(TaskAddComponent, {
            data: {task: task},
            width: '50vw' 
        });
    }
}
