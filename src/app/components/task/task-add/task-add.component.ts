import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Task } from '../../../models/task';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent implements OnInit {

    constructor(private dialogRef: MatDialogRef<TaskAddComponent>, 
                @Inject(MAT_DIALOG_DATA) private task: Task) { }

    ngOnInit() {
    }

    taskState = [
        { value: "active", viewValue: "Active" },
        { value: "inactive", viewValue: "Inactive"}
    ]

    repeatTask = [
        { value: "repeat", viewValue: "Repeat" },
        { value: "once", viewValue: "Once"}    
    ]
}
