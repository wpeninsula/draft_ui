export class Task {
    taskId: number;
    name: string;
    state: string;
    time: string;
    fileName: string;
    repeat: string;
    from: string;
    to: string;
}