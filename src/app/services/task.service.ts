import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
    constructor(private http: HttpClient) { }

    getAllTasks() {
        console.log("-getAllTasks-")
        return this.http.get<Task[]>("../../assets/json/tasks.json");
    }
}
