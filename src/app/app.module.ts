import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '@angular/cdk/layout';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

//Material
import { MatSidenavModule, MatToolbarModule, MatIconModule,
         MatListModule, MatButtonModule, MatTooltipModule,
         MatTableModule, MatPaginatorModule, MatSortModule,
         MatDialogModule, MatGridListModule, MatFormFieldModule,
         MatInputModule, MatSelectModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';

import  { appRoutes } from '../common/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { TaskComponent } from './components/task/task.component';
import { TaskService } from './services/task.service';
import { TaskViewComponent } from './components/task/task-view/task-view.component';
import { TaskDeleteComponent } from './components/task/task-delete/task-delete.component';
import { TaskAddComponent } from './components/task/task-add/task-add.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TaskComponent,
    TaskViewComponent,
    TaskDeleteComponent,
    TaskAddComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
    NgxMaterialTimepickerModule.forRoot(),
    
    //material
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    LayoutModule,
    MatButtonModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule
  ],
  providers: [TaskService],
  bootstrap: [AppComponent],
  entryComponents: [
      TaskAddComponent   
  ]
})
export class AppModule { }
